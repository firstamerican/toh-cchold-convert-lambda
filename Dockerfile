FROM public.ecr.aws/lambda/python:3.9 as base

ARG NEXUS_PYPI_USERNAME
ARG NEXUS_PYPI_PASSWORD

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.1.12

# System deps:
RUN yum update -y && \
 yum install -y make postgresql-devel python3-devel

# Copy only requirements to cache them in docker layer
COPY requirements.txt /app/requirements.txt
COPY requirements.internal.txt /app/requirements.internal.txt

# Project initialization:
WORKDIR /var/task

RUN pip3 install --upgrade pip
RUN pip3 install -r /app/requirements.txt
RUN pip3 install -r /app/requirements.internal.txt

# Creating folders, and files for a project:
COPY . /var/task

#FROM base as test
#RUN pip3 install -r /app/test-requirements.txt
#RUN python3 -m pytest

# Start the app
FROM base AS release
CMD ["main.lambda_handler"]
