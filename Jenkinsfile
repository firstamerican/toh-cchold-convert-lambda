pipeline {
    agent {
       label 'terraform_all_debian'
    }
    options {
        skipStagesAfterUnstable()
        disableConcurrentBuilds()
    }
    triggers {
        bitBucketTrigger([])
    }
    parameters {
        string(name: 'PACKAGE_NAME_BASE', defaultValue: 'toh_cchold_convert_lambda')
        string(name: 'PROJECT_KEY', defaultValue: 'toh_cchold_convert_lambda')
        string(name: 'ENVIRONMENT', defaultValue: 'toh_dev', description: 'Provide aws environment')
    }
    environment {
        NEXUS_PYPI_USERNAME = credentials('nexus_username')
        NEXUS_PYPI_PASSWORD = credentials('nexus_password')
        SSH_KEY = credentials('ssh_key')
    }
    stages {
        stage("Install orc") {
            steps {
                sh "pip install orc==0.70.0 --index-url=https://${NEXUS_PYPI_USERNAME}:${NEXUS_PYPI_PASSWORD}@nexus.firstam.io/repository/fams-pypi-group/simple --trusted-host=nexus.firstam.io"
            }
        }
        stage('Build: Docker Image') {
            steps{
                script {
                    env.COMMIT_ID = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    env.COMMIT_MESSAGE = sh(returnStdout: true, script: 'git log -1 --format=%B | head -n 1').trim()
                    env.PACKAGE_NAME_SUFFIX = "${env.COMMIT_ID}-${currentBuild.startTimeInMillis}"
                }
                sh "orc docker-build-v2 --project_key ${params.PROJECT_KEY} --branch_name ${env.BRANCH_NAME} --package_name_suffix ${env.PACKAGE_NAME_SUFFIX}"
            }
        }
        stage('Terraform: Apply') {
            steps {
                dir("tf") {
                    sh "orc apply --environment ${params.ENVIRONMENT} --project_key ${params.PROJECT_KEY}"
                }
            }
        }
        stage('Terraform: Upload to S3') {
            steps {
                dir("tf") {
                    sh "orc package-v2 --project_key ${params.PROJECT_KEY} --package_name_prefix ${params.PACKAGE_NAME_BASE} --branch_name ${env.BRANCH_NAME} --package_name_suffix ${env.PACKAGE_NAME_SUFFIX} --directory ."
                }
            }
        }
        stage('Save to Manifest') {
            when {
                expression {
                    env.BRANCH_NAME == 'development' || env.BRANCH_NAME == 'main' || env.BRANCH_NAME == 'master' || env.BRANCH_NAME ==~ /^release.*/
                }
            }
            steps {
                update_manifest_v2()
            }
        }
    }
}
