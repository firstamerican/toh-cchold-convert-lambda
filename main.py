from src.config.config import CAMUNDA_AUTH
from src.camunda_client import CamundaClient
from src.order_client import OrderClient, OrderClientWrapper
from src.urlrunner import UrlNotFoundException
from src.extractor import Extractor, extractor_fieldnames
from src.csw_writer import CsvWriter
from src.ccholdreleasesender import CCHoldReleaseSender

import logging
import boto3
import time


BUCKET_NAME = "dev-unify-lambda-bucket-us-east-1"
BUCKET_FILEPATH = f'upload/cchold/cchold_{time.strftime("%Y%m%d-%H%M%S")}.csv'
FILE_NAME = f'/tmp/cchold.csv'


def mylogging(event):
    logging.basicConfig()

    if 'debug' in event:
        logging.getLogger().setLevel(logging.DEBUG)
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)
        requests_log.propagate = True


def print_file(filename):
    with open(filename, 'rb') as file:
        print("READING FILE")
        for line in file:
            print(f"LINE: {line}")


def save_file_to_s3(filename):
    print(f"saving to:{filename}")
    s3 = boto3.resource('s3')
    s3object = s3.Object(BUCKET_NAME, BUCKET_FILEPATH)
    print_file(filename)
    result = s3object.put(Body=open(filename, 'rb'))
    return result


def get_number_of_instances(event, instances_list):
    number_of_instances = len(instances_list)
    if 'number_of_instances' in event:
        number_of_instances = event['number_of_instances']
        print(f"limiting number of instances to: {number_of_instances}")
        return event['number_of_instances']
    return number_of_instances


def lambda_handler(event, context):
    mylogging(event)
    print(f"STARTING toh-cchhold-convert-lambda handler with event: {event}")
    should_release = 'action' in event and event['action'] == 'cc_hold_release'
    print(f"should_release_cchold=={should_release}")

    cc = CamundaClient(CAMUNDA_AUTH)
    instances_list = cc.get_camunda_data()
    oc = OrderClientWrapper()

    file = open(FILE_NAME, 'w', newline='')
    csv_writer = CsvWriter(file, extractor_fieldnames)

    number_of_instances = get_number_of_instances(event, instances_list)
    for i in range(number_of_instances):
        instance_id = instances_list[i]
        camunda_order = cc.get_camunda_pid_details(instance_id)
        print('\n#--------------------------------------------------------')
        print('# PID DETAILS:')
        print(camunda_order)
        print('ORDER DATA:')

        try:
            order_api_data = oc.request_order_data(camunda_order['orderId'])
            extractor = Extractor()
            json_data = extractor.extract(order_api_data, camunda_order['productId'])
            csv_writer.row(json_data)

            if should_release:
                oc.change_status_to_cchold(camunda_order['orderId'], camunda_order['productId'])
                sender = CCHoldReleaseSender(CamundaClient(CAMUNDA_AUTH))
                sender.send(camunda_order['pid'])

        except UrlNotFoundException as e:
            print("NO ORDER FOUND!: ")
        except Exception as e:
            print(f"ERROR: Unhandled Exception: {str(e)}")
        finally:
            print('#--------------------------------------------------------')

    file.close()
    # save_file_to_s3(FILE_NAME)

    print(f"ENDING toh-cchhold-convert-lambda.")
    return instances_list


if __name__ == '__main__':
    lambda_handler({'number_of_instances': 1, 'debug': "True"},  None)
