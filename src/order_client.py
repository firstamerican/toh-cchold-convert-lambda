from requests.exceptions import HTTPError
from rest_client import OrderClient
from src.urlrunner import UrlNotFoundException
from src.config.logger import logger
from src.config.config import ORDER_BASE_URL, TITAN_REQUEST_ORIGIN_HEADER, TITAN_DATA_REPLICATION, AUTH_CONFIG
from common_data.orders import ProductStatus

additional_headers = {
    TITAN_REQUEST_ORIGIN_HEADER: TITAN_DATA_REPLICATION
}


class OrderClientWrapper:
    def __init__(self):
        self.order_client = OrderClient(ORDER_BASE_URL, AUTH_CONFIG, logger, additional_headers)

    def request_order_data(self, order_id):
        try:
            return self.order_client.get_order_by_id(order_id)
        except HTTPError as e:
            if e.response.status_code == 404:
                raise UrlNotFoundException
            else:
                raise e

    def change_status_to_cchold(self, order_id, product_id):
        try:
            return self.order_client.edit_order_product(order_id=order_id,
                                                        product_id=product_id,
                                                        product_status=ProductStatus.CC_HOLD.value)
        except HTTPError as e:
            if e.response.status_code == 404:
                raise UrlNotFoundException
            else:
                raise e
