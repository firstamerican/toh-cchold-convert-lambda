extractor_fieldnames = ['customerNumber','branchNumber','orderNumber','productCreatedDate','productCode', 'productPayments']


class Extractor:
    def __init__(self):
        self.ret = {}
        pass

    def get_borrowers(self, json):
        self.ret['parties'] = []
        for party in json['data']['parties']:
            if party['partyType'] == "Borrower":
                self.ret['parties'].append({'lastName':  party['lastName']})

        return self.ret

    def get_product_data_from_json(self, json, product_id, ):
        for product in json['data']['products']:
            if product['id'] == product_id:
                self.ret['productCode'] = product['productCode']
                self.ret['productCreatedDate'] = product['createdDate']
                if 'payments' in product:
                    self.ret['productPayments'] = product['payments']

        return self.ret

    def get_order_data(self, order_data_json, product_id):
        self.ret['orderNumber'] = order_data_json['orderNumber']
        self.ret['customerNumber'] = order_data_json['customerNumber']
        self.ret['branchNumber'] = order_data_json['branchNumber']
        self.get_product_data_from_json(order_data_json, product_id)

        print(self.ret, end='\n\n')
        return self.ret

    def extract(self, order_data_json, product_id):
        return self.get_order_data(order_data_json, product_id)
