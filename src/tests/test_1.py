from main import Extractor, CamundaClient, extractor_fieldnames
from src.csw_writer import CsvWriter

json = {"customerNumber": 1259904,
        "branchNumber": 2,
        "orderNumber": 91424829,
        "data": {
            "products": [
                {"id": "2f8b7e7444204424aa7afeeeeedb5474", "rush": "false",
                 "camunda": {"businessKey": "PRODUCT_91424829_0_42",
                             "processInstanceId": "b6d60d0e-bb28-11ec-a505-0210f32e5c1b",
                             "processDefinitionId": "Appraisal:62:b3b8f4d4-b4c6-11ec-872b-0287480212b7",
                             "processDefinitionKey": "Appraisal"}, "features": [
                    {"id": "8b3a3cd90ba14bd9a3e6616bbd081f0a", "fees": [
                        {"id": "980ed8ce14cf480893c9a9e14c7ba874", "fee": "500",
                         "feeId": "8528ee7e83d64e63b6570a4465c2bdca", "status": "Draft", "feeCode": "42",
                         "ordered": "2022-04-13T12:53:11Z", "primary": "true",
                         "branchId": "6e073d41052c42bc927bfc1f70182fdf",
                         "modifiedBy": "f114cbdf-d7b2-4a1e-ad39-07851d17b346"}], "name": "Appraisal 42",
                     "primary": "true", "standard": "false", "featureId": "e1c2871cf18146ac94c2b8c53dad95eb",
                     "featureCode": "TN096", "workflowSeriesId": "null"},
                    {"id": "f0f7a1dc37fb430da49d8d5ea6be76db", "fees": [],
                     "featureId": "d1ad29c9f9814ad3b4c624a2d20f4c83", "featureCode": "TN001",
                     "workflowSeriesId": "null"}], "feeModel": "Standard", "payments": [
                    {"id": "45c6cb3b1786412caeab6989bc3fe732", "date": "2022-04-13T12:56:42Z", "amount": 550.0,
                     "method": "Credit Card"},
                    {"id": "8bfc02269fd6454e9a5cab3f1663de7a", "date": "2022-04-14T13:37:31Z", "amount": -9.99,
                     "method": "Credit Card Refund"}], "lenderFee": 500.0,
                 "productId": "ecaa6b4b0b6041ba89c8bb531516d488", "createdDate": "2022-04-13T12:53:18Z",
                 "productCode": "42", "productName": "URAR:FNMA 1004 w/comp. photos",
                 "businessLine": "Valuations - AMC", "productUsage": "Primary", "gradingReview": "false",
                 "invoiceMethod": "One invoice number per invoice", "offHoldStatus": "null",
                 "productStatus": "Unassigned", "sequenceNumber": 0, "productCategory": "Appraisal",
                 "instructionReview": "false"},
                {"id": "fcf230fb74c144a18ba793950597dd70", "rush": "false",
                 "camunda": {"businessKey": "PRODUCT_91424829_1_41",
                             "processInstanceId": "b6c8c66a-bb28-11ec-a505-0210f32e5c1b",
                             "processDefinitionId": "Appraisal:62:b3b8f4d4-b4c6-11ec-872b-0287480212b7",
                             "processDefinitionKey": "Appraisal"}, "features": [
                    {"id": "23f70a164f5c49e7a89f8797e7b8bb37", "fees": [
                        {"id": "0bfa41014cbe4f8b861db6702f52d836", "fee": "450.00",
                         "feeId": "ba79db9dd7154a9d9f5892cd6b088e4e", "status": "Draft", "feeCode": "41",
                         "ordered": "2022-04-13T12:53:11Z", "primary": "true",
                         "branchId": "6e073d41052c42bc927bfc1f70182fdf",
                         "modifiedBy": "f114cbdf-d7b2-4a1e-ad39-07851d17b346"}], "name": "Appraisal 41",
                     "primary": "true", "standard": "false", "featureId": "d659a9a9f6d740ab87c6b869b6540a2d",
                     "featureCode": "TN108", "workflowSeriesId": ""},
                    {"id": "b3639a83f042462a9e99b492d04c0783", "fees": [],
                     "featureId": "d1ad29c9f9814ad3b4c624a2d20f4c83", "featureCode": "TN001",
                     "workflowSeriesId": "null"}], "feeModel": "Standard", "payments": [
                    {"id": "e8915675f5d74487be43b72138d4bd93", "date": "2022-04-13T12:57:50Z", "amount": 400.0,
                     "method": "Credit Card"}], "lenderFee": 450.0, "productId": "a8e2a2fff8ca4d29ae55584afbcdb928",
                 "createdDate": "2022-04-13T12:53:18Z", "productCode": "41",
                 "productName": "FNMA 2055 exterior - drive by", "businessLine": "Valuations - AMC",
                 "productUsage": "Primary", "gradingReview": "false", "invoiceMethod": "One invoice number per invoice",
                 "offHoldStatus": "null", "productStatus": "Unassigned", "sequenceNumber": 1,
                 "productCategory": "Appraisal", "instructionReview": "false"},
                {"id": "ca2d67ee746745a4b03080a23f205a7b", "rush": "false",
                 "camunda": {"businessKey": "PRODUCT_91424829_2_42",
                             "processInstanceId": "e2507749-bb29-11ec-a505-0210f32e5c1b",
                             "processDefinitionId": "Appraisal:62:b3b8f4d4-b4c6-11ec-872b-0287480212b7",
                             "processDefinitionKey": "Appraisal"}, "features": [
                    {"id": "290616906d664c9c8439225cce4b69df", "fees": [
                        {"id": "4ccd017dea794725a2725f074a07fb2d", "fee": "500",
                         "feeId": "8528ee7e83d64e63b6570a4465c2bdca", "status": "Draft", "feeCode": "42",
                         "ordered": "2022-04-13T13:01:32Z", "primary": "true",
                         "branchId": "6e073d41052c42bc927bfc1f70182fdf",
                         "modifiedBy": "f114cbdf-d7b2-4a1e-ad39-07851d17b346"}], "name": "Appraisal 42",
                     "primary": "true", "standard": "false", "featureId": "e1c2871cf18146ac94c2b8c53dad95eb",
                     "featureCode": "TN096", "workflowSeriesId": "null"},
                    {"id": "113acfbf4add45919be7322eaf4a680f", "fees": [],
                     "featureId": "d1ad29c9f9814ad3b4c624a2d20f4c83", "featureCode": "TN001",
                     "workflowSeriesId": "null"}], "feeModel": "Standard", "lenderFee": 500.0,
                 "productId": "ecaa6b4b0b6041ba89c8bb531516d488", "createdDate": "2022-04-13T13:01:33Z",
                 "productCode": "42", "productName": "URAR:FNMA 1004 w/comp. photos",
                 "businessLine": "Valuations - AMC", "productUsage": "Primary", "gradingReview": "false",
                 "invoiceMethod": "One invoice number per invoice", "offHoldStatus": "null",
                 "productStatus": "Unassigned", "sequenceNumber": 2, "productCategory": "Appraisal",
                 "instructionReview": "false"},
                {"id": "78fd82830a6547caa030901943171b83", "rush": "false",
                 "camunda": {"businessKey": "PRODUCT_91424829_3_42",
                             "processInstanceId": "48275dce-bb2b-11ec-a505-0210f32e5c1b",
                             "processDefinitionId": "Appraisal:62:b3b8f4d4-b4c6-11ec-872b-0287480212b7",
                             "processDefinitionKey": "Appraisal"}, "features": [
                    {"id": "1c25dd6a762f4eddb225b85a6708c48a", "fees": [
                        {"id": "d847992de0a84af39991e5b0070fdb8a", "fee": "500",
                         "feeId": "8528ee7e83d64e63b6570a4465c2bdca", "status": "Draft", "feeCode": "42",
                         "ordered": "2022-04-13T13:11:31Z", "primary": "true",
                         "branchId": "6e073d41052c42bc927bfc1f70182fdf",
                         "modifiedBy": "f114cbdf-d7b2-4a1e-ad39-07851d17b346"}], "name": "Appraisal 42",
                     "primary": "true", "standard": "false", "featureId": "e1c2871cf18146ac94c2b8c53dad95eb",
                     "featureCode": "TN096", "workflowSeriesId": "null"},
                    {"id": "c9e386b0235840c595ee38751501ed60", "fees": [],
                     "featureId": "d1ad29c9f9814ad3b4c624a2d20f4c83", "featureCode": "TN001",
                     "workflowSeriesId": "null"}], "feeModel": "Standard", "lenderFee": 500.0,
                 "productId": "ecaa6b4b0b6041ba89c8bb531516d488", "createdDate": "2022-04-13T13:11:32Z",
                 "productCode": "42", "productName": "URAR:FNMA 1004 w/comp. photos",
                 "businessLine": "Valuations - AMC", "productUsage": "Primary", "gradingReview": "false",
                 "invoiceMethod": "One invoice number per invoice", "offHoldStatus": "null",
                 "productStatus": "Unassigned", "sequenceNumber": 3, "productCategory": "Appraisal",
                 "instructionReview": "false"}], "lenderDue": "2022-04-21T03:59:00Z", "loanPurpose": "Other",
            "trackingNum": "", "propertyType": "2-4 Multi Family", "businessLines": ["Valuations - AMC"],
            "secondOpinion": "false",
            "mailingAddress": {"id": "c275a3203bca401a832a09ebba966bbd", "city": "Hamilton", "zipCode": "35570-3000",
                               "streetOne": "123 Military St N", "streetTwo": "",
                               "county": {"id": "01-093", "fips": "01-093", "name": "Marion", "stateId": "AL-01"},
                               "state": {"id": "AL-01", "fips": "AL-01", "name": "Alabama", "abbreviation": "AL"}},
            "propertyAddress": {"id": "b1fa75cb9ce44f6186e9ba7151bedf66", "city": "Hamilton", "zipCode": "35570-3000",
                                "streetOne": "123 Military St N", "streetTwo": "", "municipality": "",
                                "county": {"id": "01-093", "fips": "01-093", "name": "Marion", "stateId": "AL-01"},
                                "state": {"id": "AL-01", "fips": "AL-01", "name": "Alabama", "abbreviation": "AL"}},
            "transactionType": "None", "creditCardOption": "Credit Card with Hold", "customerContacts": [],
            "customerBusinessLines": ["Originations", "Servicing", "Valuations - AMC", "Valuations - Staff"],
            "modifiedMortgageAmount": "", "unpaidPrincipalBalance": "",
            "normalizedPropertyAddress": {"apn": "", "zip": "35570", "city": "Hamilton", "state": "AL",
                                          "county": "Marion", "country": "US", "zipPlus": "3000", "fipsCode": "01093",
                                          "latitude": 34.143693, "longitude": -87.98844, "propertyId": "0",
                                          "streetName": "MILITARY", "streetType": "ST", "unitNumber": "",
                                          "fullAddress": "123 Military St N, , Hamilton AL 35570-3000 Marion",
                                          "zipPlusFour": "35570-3000", "addressLine1": "123 Military St N",
                                          "addressLine2": "", "streetNumber": "123", "unitDesignation": "",
                                          "streetPreDirection": "", "streetPostDirection": "N"},
            "creditCardOrderHoldReleaseEventSent": "true"}, "dataFields": {"fields": []}, "orderMetadata": {},
        "createdDate": "2022-04-13T12:53:18Z", "lastModifiedDate": "2022-04-14T13:37:31Z",
        "createdByUserId": "f114cbdf-d7b2-4a1e-ad39-07851d17b346",
        "lastModifiedByUserId": "fbcc2632-0c8c-4b55-9ac7-b9f88b2b1f97"}

parties_json = {
    "data": {
        "parties": [
            {
                "id": "905a383ff3394aa8893aaa35e33ecbbe",
                "address": {
                    "id": "c0454666466e405ebbd0bb7191ff956c",
                    "city": "",
                    "zipCode": "",
                    "streetOne": ""
                },
                "lastName": "TestB",
                "firstName": "TestA",
                "partyType": "Borrower"
            }
        ]
    }
}


def test_get_product_data_from_json():
    extractor = Extractor()
    ret = extractor.extract(json, "2f8b7e7444204424aa7afeeeeedb5474")
    assert ret == {'customerNumber': 1259904,
                   'branchNumber': 2,
                   'orderNumber': 91424829,
                   'productCreatedDate': '2022-04-13T12:53:18Z',
                   'productCode': '42',
                   'productPayments': [
                       {'id': '45c6cb3b1786412caeab6989bc3fe732', 'date': '2022-04-13T12:56:42Z', 'amount': 550.0,
                        'method': 'Credit Card'},
                       {'id': '8bfc02269fd6454e9a5cab3f1663de7a', 'date': '2022-04-14T13:37:31Z', 'amount': -9.99,
                        'method': 'Credit Card Refund'}]
                   }


def test_csw_writer():
    import io
    file = io.StringIO()
    c = CsvWriter(file, extractor_fieldnames)
    c.row({'customerNumber': 584, 'branchNumber': 0, 'orderNumber': 90609454,
           'productCreatedDate': '2021-07-15T20:12:34Z', 'productCode': '42'})
    file.seek(0)
    data = file.read()
    refdata = "customerNumber,branchNumber,orderNumber,productCreatedDate,productCode,productPayments\r\n584,0,90609454,2021-07-15T20:12:34Z,42,\r\n"
    assert data == refdata


# def test_get_borrowers(self):
#     extractor = Extractor()
#     ret = extractor.get_borrowers(parties_json)
#     assert ret == {'parties': [{'lastName': 'TestB'}]}
#
# @patch('cch.requests.post')
# def test_cc_hold_sender(self, mock_post):
#     result = [
#         {
#             "resultType": "Execution",
#             "execution": {
#                 "id": "e31cc75a-bb29-11ec-a505-0210f32e5c1b",
#                 "processInstanceId": "e2507749-bb29-11ec-a505-0210f32e5c1b",
#                 "ended": True,
#                 "tenantId": None
#             },
#             "processInstance": None
#         }
#     ]
#     mock_post.return_value = Mock(status_code=200, json=lambda: {})
#     mock_post.return_value.json.return_value = result
#
#     cc = CamundaClient(None)
#     sender = CCHoldReleaseSender(cc)
#     ret = sender.send("e2507749-bb29-11ec-a505-0210f32e5c1b")
#     assert ret is True


# def test_csw_writer2():
#     import io
#     file = io.StringIO()
#     c = CsvWriter(file, extractor_fieldnames)
#
#     data = {
#         'orderNumber': 90501430, 'productCode': '42', 'productCreatedDate': '2021-05-12T13:21:51Z',
#         'productPayments': [
#             {'id': '248a2ebc0c53462d9e944703a87a7781', 'date': '2021-04-26T15:42:26Z', 'amount': 500.0, 'method': 'Credit Card'},
#             {'id': 'daee2a56a69f48b192466f7be4b229c7', 'date': '2021-12-05T14:14:38Z', 'amount': 425.0, 'method': 'Credit Card'}],
#         'customerNumber': 1145361, 'branchNumber': 1}
#
#     c.row(data)
#
#     file.seek(0)
#     data = file.read()
#     refdata = "customerNumber,branchNumber,orderNumber,productCreatedDate,productCode,productPayments\r\n584,0,90609454,2021-07-15T20:12:34Z,42,\r\n"
#     assert(data == refdata)
