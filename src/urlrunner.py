import requests
from src.config.config import TITAN_REQUEST_ORIGIN_HEADER, TITAN_DATA_REPLICATION, AUTH_CONFIG


class UrlNotFoundException(Exception):
    pass


class UrlRunner:

    def __init__(self):
        pass

    def headers(self):
        return None

    def req_get(self, url, auth):
        response = None
        try:
            headers = self.headers()
            response = requests.get(url=url,
                                    params={},
                                    auth=auth,
                                    headers=headers)

        except Exception as e:
            print(f'url: {url}')
            raise e

        if response.status_code == 401:
            raise Exception(f'AUTHORIZATION ISSUE - 401')

        if response.status_code == 404:
            raise UrlNotFoundException()

        if response.status_code != 200:
            raise Exception(f'status_code is {response.status_code}')

        return response.json()

    def req_post(self, url, json, auth):
        response = None
        try:
            response = requests.post(url=url,
                                     json=json,
                                     params={},
                                     auth=auth,
                                     headers=self.headers())

        except Exception as e:
            print(f'url: {url}')
            raise e

        if response.status_code == 401:
            raise Exception(f'AUTHORIZATION ISSUE - 401')

        if response.status_code == 404:
            raise UrlNotFoundException()

        if response.status_code != 200:
            raise Exception(f'status_code is {response.status_code}')

        return response.json()