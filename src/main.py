#import os
import logging
import jsonpickle

logger = logging.getLogger()
logger.setLevel(logging.INFO)
patch_all()


def lambda_handler(event, context):
    #logger.info('## ENVIRONMENT VARIABLES\r' + jsonpickle.encode(dict(**os.environ)))
    logger.info('## EVENT\r' + jsonpickle.encode(event))
    logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    
    return {"My Success"}

