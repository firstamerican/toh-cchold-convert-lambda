from src.config.config import CAMUNDA_ENGINE_URL
from src.urlrunner import UrlRunner


class CamundaClient:
    def __init__(self, auth):
        self.auth = auth
        self.base_url = CAMUNDA_ENGINE_URL
        self.url_runner = UrlRunner()
        print(f"CAMUNDA_ENGINE_URL: {self.base_url}")

    def get_camunda_data(self):
        url = self.base_url + '/process-instance?active=true&processDefinitionKey=Appraisal&activityIdIn=OrderPaid'

        response = self.url_runner.req_get(url, auth=self.auth)

        eid_list = [element['id'] for element in response]
        print(eid_list, end='\n\n')
        return eid_list

    def get_camunda_pid_details(self, pid):
        url = self.base_url + f'/process-instance/{pid}/variables'
        response = self.url_runner.req_get(url, auth=self.auth)
        ret = {}
        ret['pid'] = pid
        ret['productId'] = response['productId']['value']
        ret['orderId'] = response['orderId']['value']
        return ret

    def send_message(self, pid):
        url = self.base_url + f'/message'
        json = {
            "messageName": "Appraisal.CCHoldReleased",
            "processInstanceId": pid,
            "resultEnabled": "true"
        }

        response = self.url_runner.req_post(url=url, json=json, auth=self.auth)
        return response