import csv


class CsvWriter:
    def __init__(self, file, fieldnames):
        self.file = file
        self.writer = csv.DictWriter(self.file, fieldnames=fieldnames)
        self.writer.writeheader()


    def row(self, row):
        self.writer.writerow(row)


