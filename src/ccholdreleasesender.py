class CCHoldReleaseSender:
    def __init__(self, camunda_client):
        self.camunda_client = camunda_client

    def send(self, pid):
        res = self.camunda_client.send_message(pid)

        try:
            if res[0]["execution"]["ended"] is True:
                print("Ok")
        except Exception as e:
            print(f'ERROR: CCHoldReleaseSender response Exception!')
            raise e

        return True
