from dataclasses import dataclass
from requests.auth import HTTPBasicAuth
import os

from src.config.logger import logger
from fams_common_logging.loggers import Level

IS_PROD = os.getenv('DEBUG', 'true') != 'true'


def get_config_value(env_var_name, default):
    try:
        value = os.environ[env_var_name]
    except KeyError as exception:
        if IS_PROD:
            logger.exception(exception, extra_errors=[f'Missing configuration variable: {env_var_name}!'])
            raise exception
        else:
            logger.exception(
                exception,
                extra_errors=[f'Missing configuration variable: {env_var_name}. ' f'Will use default value.'],
                level=Level.WARN
            )
            return default
    return value


# Headers
TITAN_REQUEST_ORIGIN_HEADER = "Titan-Request-Origin"

# Header values
TITAN_DATA_REPLICATION = "TitanDataReplication"

@dataclass
class AuthConfig:
    cognito_url: str
    client_id: str
    client_secret: str
    scope: str


CAMUNDA_USERNAME = get_config_value('CAMUNDA_USERNAME', 'camunda')
CAMUNDA_PASSWORD = get_config_value('CAMUNDA_PASSWORD', 'CA6M8b>RF-R>wach')
CAMUNDA_AUTH = HTTPBasicAuth(CAMUNDA_USERNAME, CAMUNDA_PASSWORD)
CAMUNDA_ENGINE_URL = get_config_value('CAMUNDA_ENGINE_URL', "https://camunda.dev-fams-toh.io/engine-rest")
ORDER_BASE_URL = get_config_value('ORDER_API_URL', "https://order-api.dev-fams-toh.io/")
COGNITO_URL = get_config_value('COGNITO_URL', "https://fams-sso-dev.auth.us-east-1.amazoncognito.com/")
CLIENT_ID = get_config_value('CLIENT_ID', "3r3blmts8kq9rmg5egpnan80iu")
CLIENT_SECRET = get_config_value('CLIENT_SECRET', "m9k4bb48vurq9rgnd8n868r72tgl7vjngavuuv31s5kapq7sbie")
SCOPE = get_config_value('SCOPE', "toh_order_api/admin toh_order_api/crud")


AUTH_CONFIG = AuthConfig(
    cognito_url=COGNITO_URL,
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET,
    scope=SCOPE,
)
