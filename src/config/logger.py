from fams_common_logging.loggers import build_logger, Level

logger = build_logger(name=__name__, level=Level.WARN.value)