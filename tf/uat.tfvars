environment = "uat"
profile     = "toh_nonprod"
region      = "us-west-2"

tags = {
  Name          = "toh_cchold_convert_lambda"
  Application   = "FTOH"
  ApplicationID = "APP0004664" // TODO: Replace
  Environment   = "uat"
}

availability_zones = [
  "us-west-2a",
  "us-west-2b"
]

network_resources = {
  vpc_name            = "fams-sharedservices-nonprod-vpc",
  public_subnet_name  = "fams-sharedservices-nonprod-public",
  private_subnet_name = "fams-sharedservices-nonprod-private"
}

lambda_environment_variables = {
  PROFILE_NAME = "toh_nonprod"
  REGION       = "us-west-2"
}
