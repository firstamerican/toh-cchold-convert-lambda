locals {
  lambda_role_name           = "${var.environment}_${var.lambda_function_name}_role"
  lambda_logging_policy_name = "${var.environment}_${var.lambda_function_name}_logging_policy"
  lambda_kms_policy_name     = "${var.environment}_${var.lambda_function_name}_kms_policy"
  lambda_policy_name         = "${var.environment}_${var.lambda_function_name}_policy"
}

  module "allow_all_outbound" {
    source      = "git::ssh://git@bitbucket.org/firstamerican/tf-security-group.git?ref=release-0.1.0"
    name        = "${var.environment}-toh-cchold-convert-lamdba-allow_all_outbound"
    description = "Allow outbound traffic"
    vpc_id      = data.aws_vpc.vpc.id

    egresses = [
      {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = [
          "0.0.0.0/0"
        ]
        security_groups = null
        self            = null
      }
    ]
    ingresses = [
      {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = [
          "0.0.0.0/0"
        ]
        security_groups = null
        self            = null
      }
    ]
    tags = var.tags
  }

resource "aws_lambda_function" "toh_cchold_convert_lambda" {
  function_name = "${var.profile}_toh_cchold_convert_lambda"
  role          = aws_iam_role.iam_for_lambda.arn
  package_type  = "Image"
  image_uri     = data.aws_ssm_parameter.docker_image.value


  # Optional
  memory_size = var.lambda_memory_size
  timeout     = var.lambda_timeout
  environment {
    variables = {
      DEBUG                  = false
      ORDER_API_URL          = data.aws_ssm_parameter.order_api_url.value
      PRODUCT_API_URL        = data.aws_ssm_parameter.product_api_url.value
      COGNITO_URL            = data.aws_ssm_parameter.cognito_url.value
      CLIENT_ID              = data.aws_ssm_parameter.toh_cchold_convert_lambda_client_id.value
      CLIENT_SECRET          = data.aws_ssm_parameter.toh_cchold_convert_lambda_client_secret.value
      SCOPE                  = data.aws_ssm_parameter.toh_cchold_convert_lambda_client_scope.value
      CAMUNDA_ENGINE_URL     = data.aws_ssm_parameter.camunda_engine_url.value
      CAMUNDA_USERNAME       = data.aws_ssm_parameter.camunda_username.value
      CAMUNDA_PASSWORD       = data.aws_ssm_parameter.camunda_password.value
      REGION                 = var.region
      ENV                    = var.environment
    }
  }

    vpc_config {
    subnet_ids         = data.aws_subnet.private_subnets.*.id
    security_group_ids = [module.allow_all_outbound.security_group_id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.lambda_logs,
    aws_cloudwatch_log_group.this
  ]
}



