variable "environment" {
  type        = string
  description = "The environment we are working within."
}

variable "profile" {
  type        = string
  description = "The profile name."
}

variable "region" {
  type        = string
  description = "The region to deploy the lambda."
}

variable "tags" {
  type        = map(any)
  description = "The tags associated with the resource in AWS."
}

variable "lambda_function_name" {
  type        = string
  description = "The function name of the lambda."
  default     = "toh_cchold_convert_lambda"
}

variable "lambda_memory_size" {
  type        = number
  description = "The amount of memory to allocate to the lambda."
  default     = 128
}

variable "lambda_timeout" {
  type        = number
  description = "The number of seconds before the lambda times out."
  default     = 900
}

variable "lambda_reserved_concurrent_executions" {
  type        = number
  description = "Amount of reserved concurrent executions for this lambda function. A value of 0 disables lambda from being triggered and -1 removes any concurrency limitations. Defaults to Unreserved Concurrency Limits -1."
  default     = -1
}

variable "lambda_environment_variables" {
  type        = map(any)
  description = "Map of environment variables that are accessible from the function code during execution."
  default     = {}
}

variable "lambda_log_retention" {
  type        = string
  description = "The number of days to keep the lambda logs"
  default     = "30"
}

variable "is_lambda_vpc_access_enabled" {
  type        = bool
  description = ""
  default     = true
}

variable "availability_zones" {
  type        = list(any)
  description = "The list of availability zones"
}

variable "network_resources" {
  type        = map(any)
  description = "A map of network resources available in the VPC"
}

variable "ssm_repo_prefix" {
  type        = string
  description = "repo prefix variable"
  default = "cchold_convert_lambda"
}

# reuse from titan_order_lambda_topic_worker
variable "cognito_prefix" {
  type        = string
  description = "repo prefix variable"
  default     = "titan_order_lambda_topic_worker"
}
