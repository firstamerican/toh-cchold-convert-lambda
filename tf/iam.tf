resource "aws_iam_role" "iam_for_lambda" {
  name        = local.lambda_role_name
  description = "role for toh_cchold_convert_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "policy_for_lambda" {
  name        = local.lambda_policy_name
  description = "policy for toh_cchold_convert_lambda"

  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Action":[
        "ses:*"
      ],
      "Resource":"*"
    }
  ]
}
EOF
}
resource "aws_iam_role_policy_attachment" "default" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_kms_policy_name.arn
}

resource "aws_iam_policy" "lambda_kms_policy_name" {
  name = local.lambda_kms_policy_name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "kms:*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

locals {
  titan_local_bucket         = "${var.environment}-unify-lambda-bucket-${var.region}"
}

resource "aws_iam_policy" "toh_cchold_convert_lambda_s3_bucket_policy" {
  name        = "${var.environment}_toh_cchold_convert_lambda_s3_bucket_policy"
  path        = "/"
  description = "IAM policy for S3 access"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:*"
            ],
            "Effect": "Allow",
            "Resource": [
              "arn:aws:s3:::${local.titan_local_bucket}/*",
              "arn:aws:s3:::${local.titan_local_bucket}"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s3_attachment" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.toh_cchold_convert_lambda_s3_bucket_policy.arn
}

resource "aws_iam_policy" "toh_cchold_convert_lambda_network_interface_policy" {
  name = "${var.environment}_toh_cchold_convert_lambda_nic_policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "ec2:DescribeNetworkInterfaces",
          "ec2:CreateNetworkInterface",
          "ec2:DeleteNetworkInterface",
          "ec2:DescribeInstances",
          "ec2:AttachNetworkInterface"
        ],
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "nic_attachment" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.toh_cchold_convert_lambda_network_interface_policy.arn
}

