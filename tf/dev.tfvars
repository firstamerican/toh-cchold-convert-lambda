environment = "dev"
profile     = "toh_dev"
region      = "us-east-1"
tags = {
  Name          = "toh_cchold_convert_lambda"
  Application   = "FTOH"
  ApplicationID = "APP0004664" // TODO: Replace
  Environment   = "dev"
}
availability_zones = [
  "us-east-1a",
  "us-east-1b"
]
network_resources = {
  vpc_name            = "shared-services-vpc"
  public_subnet_name  = "shared-services-public"
  private_subnet_name = "shared-services-private"
}
lambda_environment_variables = {
  PROFILE_NAME = "toh_dev"
  REGION       = "us-east-1"
}
