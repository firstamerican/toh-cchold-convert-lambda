environment = "test"
profile     = "toh_dev"
region      = "us-west-2"

tags = {
  Name          = "toh_cchold_convert_lambda"
  Application   = "FTOH"
  ApplicationID = "APP0004664" // TODO: Replace
  Environment   = "test"
}

availability_zones = [
  "us-west-2a",
  "us-west-2b"
]

network_resources = {
  vpc_name            = "fams-sharedservices-dev-vpc"
  public_subnet_name  = "fams-sharedservices-dev-public"
  private_subnet_name = "fams-sharedservices-dev-private"
}

lambda_environment_variables = {
  PROFILE_NAME = "toh_dev"
  REGION       = "us-west-2"
}
