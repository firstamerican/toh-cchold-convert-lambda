data "aws_ssm_parameter" "camunda_engine_url" {
  name = "/${var.environment}/toh/camunda/engine_url"
}

data "aws_ssm_parameter" "camunda_username" {
  name = "/${var.environment}/toh/camunda/username"
}

data "aws_ssm_parameter" "camunda_password" {
  name = "/${var.environment}/toh/camunda/password"
}

data "aws_ssm_parameter" "order_api_url" {
  name = "/${var.environment}/toh/order_api/url"
}

data "aws_ssm_parameter" "product_api_url" {
  name = "/${var.environment}/toh/product_api/url"
}

data "aws_ssm_parameter" "cognito_url" {
  name = "/${var.environment}/toh/${var.cognito_prefix}_client/cognito_url"
}

data "aws_ssm_parameter" "toh_cchold_convert_lambda_client_id" {
  name = "/${var.environment}/toh/${var.cognito_prefix}_client/client_id"
}

data "aws_ssm_parameter" "toh_cchold_convert_lambda_client_secret" {
  name = "/${var.environment}/toh/${var.cognito_prefix}_client/client_secret"
}

data "aws_ssm_parameter" "toh_cchold_convert_lambda_client_scope" {
  name = "/${var.environment}/toh/${var.cognito_prefix}_client/scope"
}

data "aws_ssm_parameter" "docker_image" {
  name = "/${var.environment}/toh/${var.ssm_repo_prefix}/docker_image"
}

data "aws_ssm_parameter" "build_tag" {
  name = "/${var.environment}/toh/${var.ssm_repo_prefix}/build_tag"
}

data "aws_vpc" "vpc" {
  filter {
    name = "tag:Name"

    values = [
      var.network_resources["vpc_name"],
    ]
  }
}

data "aws_subnet" "private_subnets" {
  count = length(var.network_resources)

  filter {
    name = "tag:Name"

    values = [
      "${var.network_resources["private_subnet_name"]}.${element(var.availability_zones, count.index)}",
    ]
  }
}
