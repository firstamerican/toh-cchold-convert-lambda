environment = "qa"
profile     = "toh_nonprod"
region      = "us-east-1"

tags = {
  Name          = "toh_cchold_convert_lambda"
  Application   = "FTOH"
  ApplicationID = "APP0004664" // TODO: Replace
  Environment   = "qa"
}

availability_zones = [
  "us-east-1a",
  "us-east-1b"
]

network_resources = {
  vpc_name            = "fams-sharedservices-nonprod-vpc",
  public_subnet_name  = "fams-sharedservices-nonprod-public",
  private_subnet_name = "fams-sharedservices-nonprod-private"
}

lambda_environment_variables = {
  PROFILE_NAME = "toh_nonprod"
  REGION       = "us-east-1"
}
