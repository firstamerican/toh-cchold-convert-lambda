environment = "prod"
profile     = "toh_prod"
region      = "us-west-2"

tags = {
  Name          = "toh_cchold_convert_lambda"
  Application   = "FTOH"
  ApplicationID = "APP0004664" // TODO: Replace
  Environment   = "prod"
}

availability_zones = [
  "us-west-2a",
  "us-west-2b"
]

network_resources = {
  vpc_name            = "fams-sharedservices-prod-vpc",
  public_subnet_name  = "fams-sharedservices-prod-public",
  private_subnet_name = "fams-sharedservices-prod-private"
}

lambda_environment_variables = {
  PROFILE_NAME = "toh_prod"
  REGION       = "us-west-2"
}
