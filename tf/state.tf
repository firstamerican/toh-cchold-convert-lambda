terraform {
  backend "s3" {
    profile        = "toh_dev"
    bucket         = "fams-toh-032893523956-dev-terraform-us-east-1"
    region         = "us-east-1"
    key            = "toh/cchold-convert-lambda"
    encrypt        = true
    kms_key_id     = "4df130fa-7fa7-48df-997b-7d7a34a89c30"
    dynamodb_table = "fams_toh_dev_terraform_state_lock"
  }
}